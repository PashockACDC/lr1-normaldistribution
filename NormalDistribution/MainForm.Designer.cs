﻿namespace NormalDistribution
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox_x_coord = new System.Windows.Forms.GroupBox();
            this.label_x = new System.Windows.Forms.Label();
            this.textBox_x_max_limit = new System.Windows.Forms.TextBox();
            this.label_dots1 = new System.Windows.Forms.Label();
            this.textBox_x_min_limit = new System.Windows.Forms.TextBox();
            this.groupBox_y_coord = new System.Windows.Forms.GroupBox();
            this.label_y = new System.Windows.Forms.Label();
            this.checkBox_y_auto_limit = new System.Windows.Forms.CheckBox();
            this.textBox_y_max_limit = new System.Windows.Forms.TextBox();
            this.label_dots2 = new System.Windows.Forms.Label();
            this.textBox_y_min_limit = new System.Windows.Forms.TextBox();
            this.groupBox_graphics = new System.Windows.Forms.GroupBox();
            this.radioButton_graphic_norm_distrib = new System.Windows.Forms.RadioButton();
            this.radioButton_graphic_uniform_distrib = new System.Windows.Forms.RadioButton();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox_params_norm_distrib = new System.Windows.Forms.GroupBox();
            this.label_sigma = new System.Windows.Forms.Label();
            this.textBox_sigma = new System.Windows.Forms.TextBox();
            this.label_mu = new System.Windows.Forms.Label();
            this.textBox_mu = new System.Windows.Forms.TextBox();
            this.label_error = new System.Windows.Forms.Label();
            this.groupBox_params_uniform_distrib = new System.Windows.Forms.GroupBox();
            this.label_b = new System.Windows.Forms.Label();
            this.textBox_b = new System.Windows.Forms.TextBox();
            this.label_a = new System.Windows.Forms.Label();
            this.textBox_a = new System.Windows.Forms.TextBox();
            this.pictureBox_color_choser = new System.Windows.Forms.PictureBox();
            this.numericUpDown_width = new System.Windows.Forms.NumericUpDown();
            this.label_width = new System.Windows.Forms.Label();
            this.groupBox_x_coord.SuspendLayout();
            this.groupBox_y_coord.SuspendLayout();
            this.groupBox_graphics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox_params_norm_distrib.SuspendLayout();
            this.groupBox_params_uniform_distrib.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_color_choser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_width)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_x_coord
            // 
            this.groupBox_x_coord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_x_coord.Controls.Add(this.label_x);
            this.groupBox_x_coord.Controls.Add(this.textBox_x_max_limit);
            this.groupBox_x_coord.Controls.Add(this.label_dots1);
            this.groupBox_x_coord.Controls.Add(this.textBox_x_min_limit);
            this.groupBox_x_coord.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_x_coord.Location = new System.Drawing.Point(520, 12);
            this.groupBox_x_coord.Name = "groupBox_x_coord";
            this.groupBox_x_coord.Size = new System.Drawing.Size(230, 75);
            this.groupBox_x_coord.TabIndex = 7;
            this.groupBox_x_coord.TabStop = false;
            this.groupBox_x_coord.Text = "Координата X";
            // 
            // label_x
            // 
            this.label_x.AutoSize = true;
            this.label_x.ForeColor = System.Drawing.Color.Black;
            this.label_x.Location = new System.Drawing.Point(14, 16);
            this.label_x.Name = "label_x";
            this.label_x.Size = new System.Drawing.Size(126, 13);
            this.label_x.TabIndex = 10;
            this.label_x.Text = "Пределы отображения:";
            // 
            // textBox_x_max_limit
            // 
            this.textBox_x_max_limit.Location = new System.Drawing.Point(86, 41);
            this.textBox_x_max_limit.Name = "textBox_x_max_limit";
            this.textBox_x_max_limit.Size = new System.Drawing.Size(41, 20);
            this.textBox_x_max_limit.TabIndex = 1;
            this.textBox_x_max_limit.Text = "3";
            this.textBox_x_max_limit.TextChanged += new System.EventHandler(this.textBox_x_axis_limits_TextChanged);
            // 
            // label_dots1
            // 
            this.label_dots1.AutoSize = true;
            this.label_dots1.ForeColor = System.Drawing.Color.Black;
            this.label_dots1.Location = new System.Drawing.Point(64, 44);
            this.label_dots1.Name = "label_dots1";
            this.label_dots1.Size = new System.Drawing.Size(16, 13);
            this.label_dots1.TabIndex = 7;
            this.label_dots1.Text = "...";
            // 
            // textBox_x_min_limit
            // 
            this.textBox_x_min_limit.Location = new System.Drawing.Point(16, 41);
            this.textBox_x_min_limit.Name = "textBox_x_min_limit";
            this.textBox_x_min_limit.Size = new System.Drawing.Size(41, 20);
            this.textBox_x_min_limit.TabIndex = 0;
            this.textBox_x_min_limit.Text = "-3";
            this.textBox_x_min_limit.TextChanged += new System.EventHandler(this.textBox_x_axis_limits_TextChanged);
            // 
            // groupBox_y_coord
            // 
            this.groupBox_y_coord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_y_coord.Controls.Add(this.label_y);
            this.groupBox_y_coord.Controls.Add(this.checkBox_y_auto_limit);
            this.groupBox_y_coord.Controls.Add(this.textBox_y_max_limit);
            this.groupBox_y_coord.Controls.Add(this.label_dots2);
            this.groupBox_y_coord.Controls.Add(this.textBox_y_min_limit);
            this.groupBox_y_coord.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_y_coord.Location = new System.Drawing.Point(521, 93);
            this.groupBox_y_coord.Name = "groupBox_y_coord";
            this.groupBox_y_coord.Size = new System.Drawing.Size(230, 75);
            this.groupBox_y_coord.TabIndex = 8;
            this.groupBox_y_coord.TabStop = false;
            this.groupBox_y_coord.Text = "Координата Y";
            // 
            // label_y
            // 
            this.label_y.AutoSize = true;
            this.label_y.ForeColor = System.Drawing.Color.Black;
            this.label_y.Location = new System.Drawing.Point(14, 16);
            this.label_y.Name = "label_y";
            this.label_y.Size = new System.Drawing.Size(126, 13);
            this.label_y.TabIndex = 11;
            this.label_y.Text = "Пределы отображения:";
            // 
            // checkBox_y_auto_limit
            // 
            this.checkBox_y_auto_limit.AutoSize = true;
            this.checkBox_y_auto_limit.Checked = true;
            this.checkBox_y_auto_limit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_y_auto_limit.ForeColor = System.Drawing.Color.Black;
            this.checkBox_y_auto_limit.Location = new System.Drawing.Point(147, 43);
            this.checkBox_y_auto_limit.Name = "checkBox_y_auto_limit";
            this.checkBox_y_auto_limit.Size = new System.Drawing.Size(50, 17);
            this.checkBox_y_auto_limit.TabIndex = 4;
            this.checkBox_y_auto_limit.Text = "Авто";
            this.checkBox_y_auto_limit.UseVisualStyleBackColor = true;
            this.checkBox_y_auto_limit.CheckedChanged += new System.EventHandler(this.checkBox_y_auto_limit_CheckedChanged);
            // 
            // textBox_y_max_limit
            // 
            this.textBox_y_max_limit.Enabled = false;
            this.textBox_y_max_limit.Location = new System.Drawing.Point(87, 41);
            this.textBox_y_max_limit.Name = "textBox_y_max_limit";
            this.textBox_y_max_limit.Size = new System.Drawing.Size(41, 20);
            this.textBox_y_max_limit.TabIndex = 3;
            this.textBox_y_max_limit.Text = "1";
            this.textBox_y_max_limit.TextChanged += new System.EventHandler(this.textBox_y_axis_limits_TextChanged);
            // 
            // label_dots2
            // 
            this.label_dots2.AutoSize = true;
            this.label_dots2.ForeColor = System.Drawing.Color.Black;
            this.label_dots2.Location = new System.Drawing.Point(65, 44);
            this.label_dots2.Name = "label_dots2";
            this.label_dots2.Size = new System.Drawing.Size(16, 13);
            this.label_dots2.TabIndex = 11;
            this.label_dots2.Text = "...";
            // 
            // textBox_y_min_limit
            // 
            this.textBox_y_min_limit.Enabled = false;
            this.textBox_y_min_limit.Location = new System.Drawing.Point(17, 41);
            this.textBox_y_min_limit.Name = "textBox_y_min_limit";
            this.textBox_y_min_limit.Size = new System.Drawing.Size(41, 20);
            this.textBox_y_min_limit.TabIndex = 2;
            this.textBox_y_min_limit.Text = "0";
            this.textBox_y_min_limit.TextChanged += new System.EventHandler(this.textBox_y_axis_limits_TextChanged);
            // 
            // groupBox_graphics
            // 
            this.groupBox_graphics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_graphics.Controls.Add(this.radioButton_graphic_norm_distrib);
            this.groupBox_graphics.Controls.Add(this.radioButton_graphic_uniform_distrib);
            this.groupBox_graphics.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_graphics.Location = new System.Drawing.Point(521, 209);
            this.groupBox_graphics.Name = "groupBox_graphics";
            this.groupBox_graphics.Size = new System.Drawing.Size(229, 154);
            this.groupBox_graphics.TabIndex = 9;
            this.groupBox_graphics.TabStop = false;
            this.groupBox_graphics.Text = "Графики";
            // 
            // radioButton_graphic_norm_distrib
            // 
            this.radioButton_graphic_norm_distrib.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radioButton_graphic_norm_distrib.Checked = true;
            this.radioButton_graphic_norm_distrib.ForeColor = System.Drawing.Color.Black;
            this.radioButton_graphic_norm_distrib.Image = global::NormalDistribution.Properties.Resources.Gauss;
            this.radioButton_graphic_norm_distrib.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_graphic_norm_distrib.Location = new System.Drawing.Point(15, 80);
            this.radioButton_graphic_norm_distrib.Name = "radioButton_graphic_norm_distrib";
            this.radioButton_graphic_norm_distrib.Size = new System.Drawing.Size(203, 64);
            this.radioButton_graphic_norm_distrib.TabIndex = 8;
            this.radioButton_graphic_norm_distrib.TabStop = true;
            this.radioButton_graphic_norm_distrib.UseVisualStyleBackColor = true;
            this.radioButton_graphic_norm_distrib.CheckedChanged += new System.EventHandler(this.radioButton_graphic_2_CheckedChanged);
            // 
            // radioButton_graphic_uniform_distrib
            // 
            this.radioButton_graphic_uniform_distrib.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radioButton_graphic_uniform_distrib.ForeColor = System.Drawing.Color.Black;
            this.radioButton_graphic_uniform_distrib.Image = global::NormalDistribution.Properties.Resources.Uniform;
            this.radioButton_graphic_uniform_distrib.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton_graphic_uniform_distrib.Location = new System.Drawing.Point(15, 13);
            this.radioButton_graphic_uniform_distrib.Name = "radioButton_graphic_uniform_distrib";
            this.radioButton_graphic_uniform_distrib.Size = new System.Drawing.Size(212, 64);
            this.radioButton_graphic_uniform_distrib.TabIndex = 7;
            this.radioButton_graphic_uniform_distrib.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.Interval = 0D;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MaximumAutoSize = 80F;
            chartArea1.AxisX.MinorGrid.Enabled = true;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisX.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.Silver;
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MinorGrid.Enabled = true;
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY.MinorTickMark.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY.ScaleBreakStyle.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY.TitleForeColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY2.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.MinorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.TitleForeColor = System.Drawing.Color.Silver;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea_Gauss";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(1, 1);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.EarthTones;
            series1.ChartArea = "ChartArea_Gauss";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Name = "Series_Gauss";
            series1.XValueMember = "0";
            series1.YValueMembers = "0";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(526, 386);
            this.chart1.TabIndex = 11;
            this.chart1.Text = "chart1";
            // 
            // groupBox_params_norm_distrib
            // 
            this.groupBox_params_norm_distrib.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_params_norm_distrib.Controls.Add(this.label_sigma);
            this.groupBox_params_norm_distrib.Controls.Add(this.textBox_sigma);
            this.groupBox_params_norm_distrib.Controls.Add(this.label_mu);
            this.groupBox_params_norm_distrib.Controls.Add(this.textBox_mu);
            this.groupBox_params_norm_distrib.Location = new System.Drawing.Point(521, 168);
            this.groupBox_params_norm_distrib.Name = "groupBox_params_norm_distrib";
            this.groupBox_params_norm_distrib.Size = new System.Drawing.Size(229, 41);
            this.groupBox_params_norm_distrib.TabIndex = 13;
            this.groupBox_params_norm_distrib.TabStop = false;
            // 
            // label_sigma
            // 
            this.label_sigma.AutoSize = true;
            this.label_sigma.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_sigma.Location = new System.Drawing.Point(98, 12);
            this.label_sigma.Name = "label_sigma";
            this.label_sigma.Size = new System.Drawing.Size(24, 17);
            this.label_sigma.TabIndex = 17;
            this.label_sigma.Text = "σ :";
            // 
            // textBox_sigma
            // 
            this.textBox_sigma.Location = new System.Drawing.Point(123, 13);
            this.textBox_sigma.Name = "textBox_sigma";
            this.textBox_sigma.Size = new System.Drawing.Size(45, 20);
            this.textBox_sigma.TabIndex = 6;
            this.textBox_sigma.Text = "1";
            this.textBox_sigma.TextChanged += new System.EventHandler(this.textBox_sigma_TextChanged);
            // 
            // label_mu
            // 
            this.label_mu.AutoSize = true;
            this.label_mu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_mu.Location = new System.Drawing.Point(12, 12);
            this.label_mu.Name = "label_mu";
            this.label_mu.Size = new System.Drawing.Size(24, 17);
            this.label_mu.TabIndex = 15;
            this.label_mu.Text = "μ :";
            // 
            // textBox_mu
            // 
            this.textBox_mu.Location = new System.Drawing.Point(37, 13);
            this.textBox_mu.Name = "textBox_mu";
            this.textBox_mu.Size = new System.Drawing.Size(45, 20);
            this.textBox_mu.TabIndex = 5;
            this.textBox_mu.Text = "0";
            this.textBox_mu.TextChanged += new System.EventHandler(this.textBox_mu_TextChanged);
            // 
            // label_error
            // 
            this.label_error.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_error.AutoSize = true;
            this.label_error.ForeColor = System.Drawing.Color.Red;
            this.label_error.Location = new System.Drawing.Point(1, 388);
            this.label_error.Name = "label_error";
            this.label_error.Size = new System.Drawing.Size(0, 13);
            this.label_error.TabIndex = 14;
            // 
            // groupBox_params_uniform_distrib
            // 
            this.groupBox_params_uniform_distrib.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_params_uniform_distrib.Controls.Add(this.label_b);
            this.groupBox_params_uniform_distrib.Controls.Add(this.textBox_b);
            this.groupBox_params_uniform_distrib.Controls.Add(this.label_a);
            this.groupBox_params_uniform_distrib.Controls.Add(this.textBox_a);
            this.groupBox_params_uniform_distrib.Location = new System.Drawing.Point(521, 168);
            this.groupBox_params_uniform_distrib.Name = "groupBox_params_uniform_distrib";
            this.groupBox_params_uniform_distrib.Size = new System.Drawing.Size(229, 41);
            this.groupBox_params_uniform_distrib.TabIndex = 18;
            this.groupBox_params_uniform_distrib.TabStop = false;
            this.groupBox_params_uniform_distrib.Visible = false;
            // 
            // label_b
            // 
            this.label_b.AutoSize = true;
            this.label_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_b.Location = new System.Drawing.Point(98, 12);
            this.label_b.Name = "label_b";
            this.label_b.Size = new System.Drawing.Size(24, 17);
            this.label_b.TabIndex = 17;
            this.label_b.Text = "b :";
            // 
            // textBox_b
            // 
            this.textBox_b.Location = new System.Drawing.Point(123, 13);
            this.textBox_b.Name = "textBox_b";
            this.textBox_b.Size = new System.Drawing.Size(45, 20);
            this.textBox_b.TabIndex = 6;
            this.textBox_b.Text = "1";
            this.textBox_b.TextChanged += new System.EventHandler(this.textBox_b_TextChanged);
            // 
            // label_a
            // 
            this.label_a.AutoSize = true;
            this.label_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_a.Location = new System.Drawing.Point(12, 12);
            this.label_a.Name = "label_a";
            this.label_a.Size = new System.Drawing.Size(24, 17);
            this.label_a.TabIndex = 15;
            this.label_a.Text = "a :";
            // 
            // textBox_a
            // 
            this.textBox_a.Location = new System.Drawing.Point(37, 13);
            this.textBox_a.Name = "textBox_a";
            this.textBox_a.Size = new System.Drawing.Size(45, 20);
            this.textBox_a.TabIndex = 5;
            this.textBox_a.Text = "0";
            this.textBox_a.TextChanged += new System.EventHandler(this.textBox_a_TextChanged);
            // 
            // pictureBox_color_choser
            // 
            this.pictureBox_color_choser.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox_color_choser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_color_choser.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_color_choser.Name = "pictureBox_color_choser";
            this.pictureBox_color_choser.Size = new System.Drawing.Size(10, 10);
            this.pictureBox_color_choser.TabIndex = 10;
            this.pictureBox_color_choser.TabStop = false;
            this.pictureBox_color_choser.Click += new System.EventHandler(this.pictureBox_color_choser_Click);
            // 
            // numericUpDown_width
            // 
            this.numericUpDown_width.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_width.Location = new System.Drawing.Point(666, 369);
            this.numericUpDown_width.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_width.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_width.Name = "numericUpDown_width";
            this.numericUpDown_width.Size = new System.Drawing.Size(36, 20);
            this.numericUpDown_width.TabIndex = 19;
            this.numericUpDown_width.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_width.ValueChanged += new System.EventHandler(this.numericUpDown_width_ValueChanged);
            // 
            // label_width
            // 
            this.label_width.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_width.AutoSize = true;
            this.label_width.Location = new System.Drawing.Point(528, 371);
            this.label_width.Name = "label_width";
            this.label_width.Size = new System.Drawing.Size(135, 13);
            this.label_width.TabIndex = 20;
            this.label_width.Text = "Толщина линии графика:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 403);
            this.Controls.Add(this.label_width);
            this.Controls.Add(this.numericUpDown_width);
            this.Controls.Add(this.groupBox_params_uniform_distrib);
            this.Controls.Add(this.label_error);
            this.Controls.Add(this.groupBox_params_norm_distrib);
            this.Controls.Add(this.pictureBox_color_choser);
            this.Controls.Add(this.groupBox_graphics);
            this.Controls.Add(this.groupBox_y_coord);
            this.Controls.Add(this.groupBox_x_coord);
            this.Controls.Add(this.chart1);
            this.MinimumSize = new System.Drawing.Size(778, 441);
            this.Name = "MainForm";
            this.Text = "График нормального распределения";
            this.groupBox_x_coord.ResumeLayout(false);
            this.groupBox_x_coord.PerformLayout();
            this.groupBox_y_coord.ResumeLayout(false);
            this.groupBox_y_coord.PerformLayout();
            this.groupBox_graphics.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox_params_norm_distrib.ResumeLayout(false);
            this.groupBox_params_norm_distrib.PerformLayout();
            this.groupBox_params_uniform_distrib.ResumeLayout(false);
            this.groupBox_params_uniform_distrib.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_color_choser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_width)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_x_coord;
        private System.Windows.Forms.GroupBox groupBox_y_coord;
        private System.Windows.Forms.GroupBox groupBox_graphics;
        private System.Windows.Forms.RadioButton radioButton_graphic_norm_distrib;
        private System.Windows.Forms.RadioButton radioButton_graphic_uniform_distrib;
        private System.Windows.Forms.PictureBox pictureBox_color_choser;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.TextBox textBox_x_max_limit;
        private System.Windows.Forms.Label label_dots1;
        private System.Windows.Forms.TextBox textBox_x_min_limit;
        private System.Windows.Forms.TextBox textBox_y_max_limit;
        private System.Windows.Forms.Label label_dots2;
        private System.Windows.Forms.TextBox textBox_y_min_limit;
        private System.Windows.Forms.CheckBox checkBox_y_auto_limit;
        private System.Windows.Forms.Label label_x;
        private System.Windows.Forms.Label label_y;
        private System.Windows.Forms.GroupBox groupBox_params_norm_distrib;
        private System.Windows.Forms.TextBox textBox_mu;
        private System.Windows.Forms.Label label_mu;
        private System.Windows.Forms.Label label_sigma;
        private System.Windows.Forms.TextBox textBox_sigma;
        private System.Windows.Forms.Label label_error;
        private System.Windows.Forms.GroupBox groupBox_params_uniform_distrib;
        private System.Windows.Forms.Label label_b;
        private System.Windows.Forms.TextBox textBox_b;
        private System.Windows.Forms.Label label_a;
        private System.Windows.Forms.TextBox textBox_a;
        private System.Windows.Forms.NumericUpDown numericUpDown_width;
        private System.Windows.Forms.Label label_width;
    }
}

