﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;

namespace NormalDistribution
{
    public partial class MainForm : Form
    {
        const double STEP = 1000;
        /// <summary>левый предел оси X</summary>
        private double xmin_axis_limit = -3;
        /// <summary>правый предел оси X</summary>
        private double xmax_axis_limit = 3;
        /// <summary>нижний предел оси Y</summary>
        private double ymin_axis_limit = 0;
        /// <summary>верхний предел оси Y</summary>
        private double ymax_axis_limit = 1;

        /// <summary>μ - математическое ожидание (среднее значение)</summary>
        private double mu = 0;
        /// <summary>σ — среднеквадратическое отклонение</summary>
        private double sigma = 1;

        /// <summary>нижний предел для непрерывного равномерного распределения</summary>
        private double a = 0;
        /// <summary>верхний предел для непрерывного равномерного распределения</summary>
        private double b = 1;

        /// <summary>цвет рисования графика</summary>
        Color mColorOfGraphic = Color.Maroon;

        public MainForm()
        {
            InitializeComponent();
            DrawGraph(xmin_axis_limit, xmax_axis_limit);
            (new ToolTip()).SetToolTip(checkBox_y_auto_limit,
                "Автоматически выставлять пределы по оси Y");
            (new ToolTip()).SetToolTip(textBox_mu,
                "μ - математическое ожидание (среднее значение)");
            (new ToolTip()).SetToolTip(textBox_sigma,
                "σ — среднеквадратическое отклонение (σ² — дисперсия) распределения");
        }

        /// <summary>
        /// Отрисовываем график в зависимости от выбранного радиобаттона
        /// </summary>
        /// <param name="xmin">нижний предел рисования графика по оси X</param>
        /// <param name="xmax">верхний предел рисования графика по оси X</param>
        private void DrawGraph(double xmin, double xmax)
        {
            if (radioButton_graphic_norm_distrib.Checked)
            {
                DrawGraphNormDibstib(xmin_axis_limit, xmax_axis_limit);
            }
            else
            {
                DrawGraphUniformDibstib(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Метод отрисовки графика
        /// для нормального распределения
        /// </summary>
        /// <param name="xmin">нижний предел рисования графика по оси X</param>
        /// <param name="xmax">верхний предел рисования графика по оси X</param>
        private void DrawGraphNormDibstib(double xmin, double xmax)
        {
            if (sigma == 0)
            {
                label_error.Text = "Ошибка построения! " +
                    "Среднеквадратическое отклонение (σ) не может быть равным 0!";
                return;
            }
            try
            {
                if (xmin_axis_limit > xmax_axis_limit)
                {
                    double temp = xmin_axis_limit;
                    xmin_axis_limit = xmax_axis_limit;
                    xmax_axis_limit = temp;
                }
                double stepx = (xmax_axis_limit - xmin_axis_limit) / STEP;
                Dictionary<double, double> dataForGraphic = new Dictionary<double, double>();
                for (double x = xmin; x <= xmax; x += stepx)
                {
                    dataForGraphic.Add(x, Model.MyGraphics.graphicGauss(x, mu, sigma));
                }
                chart1.ChartAreas[0].AxisX.Minimum = xmin_axis_limit;
                chart1.ChartAreas[0].AxisX.Maximum = xmax_axis_limit;
                if (checkBox_y_auto_limit.Checked)
                {
                    double min = Math.Floor(dataForGraphic.Values.Min());  // пока так
                    double max = Math.Ceiling(dataForGraphic.Values.Max());  // пока так
                    if (min != max)
                    {
                        if (min > max)
                        {
                            double temp = min;
                            min = max;
                            max = temp;
                        }
                        chart1.ChartAreas[0].AxisY.Minimum = min;
                        chart1.ChartAreas[0].AxisY.Maximum = max;
                        textBox_y_min_limit.Text = min.ToString();
                        textBox_y_max_limit.Text = max.ToString();
                    }
                    else // по умолчанию
                    {
                        chart1.ChartAreas[0].AxisY.Minimum = 0;
                        chart1.ChartAreas[0].AxisY.Maximum = 1;
                    }
                }
                else if (ymin_axis_limit != ymax_axis_limit)
                {
                    if (ymin_axis_limit > ymax_axis_limit)
                    {
                        double temp = ymin_axis_limit;
                        ymin_axis_limit = ymax_axis_limit;
                        ymax_axis_limit = temp;
                    }
                    chart1.ChartAreas[0].AxisY.Minimum = ymin_axis_limit;
                    chart1.ChartAreas[0].AxisY.Maximum = ymax_axis_limit;
                }
                else // по умолчанию
                {
                    chart1.ChartAreas[0].AxisY.Minimum = 0;
                    chart1.ChartAreas[0].AxisY.Maximum = 1;
                }
                chart1.Series[0].Points.DataBindXY(dataForGraphic.Keys, dataForGraphic.Values);
                chart1.Series[0].ChartType = SeriesChartType.Spline;
                chart1.Series[0].BorderWidth = (int)numericUpDown_width.Value;
                chart1.Series[0].Color = mColorOfGraphic;
                label_error.Text = "";
                this.Text = "График нормального распределения";
            }
            catch (Exception ex)
            {
                label_error.Text = "Ошибка построения!";
            }
        }

        /// <summary>
        /// Метод отрисовки графика
        /// для непрерывного равномерного распределения
        /// </summary>
        /// <param name="xmin">нижний предел рисования графика по оси X</param>
        /// <param name="xmax">верхний предел рисования графика по оси X</param>
        private void DrawGraphUniformDibstib(double xmin, double xmax)
        {
            if (a == b)
            {
                label_error.Text = "Ошибка построения! " +
                    "a == b";
                return;
            }
            try
            {
                double stepx = (xmax_axis_limit - xmin_axis_limit) / STEP;
                Dictionary<double, double> dataForGraphic = new Dictionary<double, double>();
                for (double x = /*xmin*/a; x <= /*xmax*/b; x += stepx)
                {
                    dataForGraphic.Add(x, Model.MyGraphics.graphicUniformDistribution(x, a, b));
                }
                if (checkBox_y_auto_limit.Checked)
                {
                    double min = Math.Floor(dataForGraphic.Values.Min());  // пока так
                    double max = Math.Ceiling(dataForGraphic.Values.Max());  // пока так
                    if (min != max)
                    {
                        if (min > max)
                        {
                            double temp = min;
                            min = max;
                            max = temp;
                        }
                        chart1.ChartAreas[0].AxisY.Minimum = min;
                        chart1.ChartAreas[0].AxisY.Maximum = max;
                        textBox_y_min_limit.Text = min.ToString();
                        textBox_y_max_limit.Text = max.ToString();
                    }
                    else // по умолчанию
                    {
                        chart1.ChartAreas[0].AxisY.Minimum = 0;
                        chart1.ChartAreas[0].AxisY.Maximum = 1;
                    }
                }
                else if (ymin_axis_limit != ymax_axis_limit)
                {
                    if (ymin_axis_limit > ymax_axis_limit)
                    {
                        double temp = ymin_axis_limit;
                        ymin_axis_limit = ymax_axis_limit;
                        ymax_axis_limit = temp;
                    }
                    chart1.ChartAreas[0].AxisY.Minimum = ymin_axis_limit;
                    chart1.ChartAreas[0].AxisY.Maximum = ymax_axis_limit;
                }
                else // по умолчанию
                {
                    chart1.ChartAreas[0].AxisY.Minimum = 0;
                    chart1.ChartAreas[0].AxisY.Maximum = 1;
                }
                chart1.ChartAreas[0].AxisX.Minimum = xmin_axis_limit;
                chart1.ChartAreas[0].AxisX.Maximum = xmax_axis_limit;
                chart1.ChartAreas[0].AxisY.Minimum = ymin_axis_limit;
                chart1.ChartAreas[0].AxisY.Maximum = ymax_axis_limit;
                chart1.Series[0].Points.DataBindXY(dataForGraphic.Keys, dataForGraphic.Values);
                chart1.Series[0].ChartType = SeriesChartType.Line;
                chart1.Series[0].BorderWidth = (int)numericUpDown_width.Value;
                chart1.Series[0].Color = mColorOfGraphic;
                label_error.Text = "";
                this.Text = "График непрерывного равномерного распределения";
            }
            catch (Exception ex)
            {
                label_error.Text = "Ошибка построения!";
            }
        }


        /**********************************
         *** ОБРАБОТЧКИКИ КНОПОК И ПРОЧ ***
         **********************************/

        /// <summary>
        /// Выбираем цвет рисования графика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_color_choser_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            DialogResult res = colorDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                mColorOfGraphic = colorDialog.Color;
                pictureBox_color_choser.BackColor = mColorOfGraphic;
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Происходит при изменении пределов отрисовки по оси X
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_x_axis_limits_TextChanged(object sender, EventArgs e)
        {
            bool tb1 = textBox_check_when_TextChanged(textBox_x_min_limit);
            bool tb2 = textBox_check_when_TextChanged(textBox_x_max_limit);
            if (tb1 && tb2)
            {
                xmin_axis_limit = Double.Parse(textBox_x_min_limit.Text.Replace('.', ','));
                xmax_axis_limit = Double.Parse(textBox_x_max_limit.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Происходит при изменении пределов отрисовки по оси Y
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_y_axis_limits_TextChanged(object sender, EventArgs e)
        {
            bool tb1 = textBox_check_when_TextChanged(textBox_y_min_limit);
            bool tb2 = textBox_check_when_TextChanged(textBox_y_max_limit);
            if (tb1 && tb2)
            {
                ymin_axis_limit = Double.Parse(textBox_y_min_limit.Text.Replace('.', ','));
                ymax_axis_limit = Double.Parse(textBox_y_max_limit.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Вкл/выкл режима автоматического выставления пределов по оси Y
        /// (нижний предел - округляется к ближайшему наибольшему целому)
        /// (верхний предел - округляется к ближайшему наименьшему целому)
        /// См. DrawGraphNormDibstib()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox_y_auto_limit_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_y_auto_limit.Checked)
            {
                textBox_y_min_limit.Enabled = false;
                textBox_y_max_limit.Enabled = false;
            }
            else
            {
                textBox_y_min_limit.Enabled = true;
                textBox_y_max_limit.Enabled = true;
            }
            DrawGraph(xmin_axis_limit, xmax_axis_limit);
        }

        /// <summary>
        /// Проверка, что в текстовое поле введено число
        /// </summary>
        /// <param name="textbox"></param>
        /// <returns></returns>
        private bool textBox_check_when_TextChanged(TextBox textbox)
        {
            double temp = 0;
            bool res = Double.TryParse(textbox.Text.Replace('.', ','), out temp);
            textbox.ForeColor = res ? Color.Black : Color.Red;
            return res;
        }

        /// <summary>
        /// Проверка, что в поле (μ - математическое ожидание) введено число.
        /// Если да, строим график по новому введённому значению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_mu_TextChanged(object sender, EventArgs e)
        {
            if (textBox_check_when_TextChanged(textBox_mu))
            {
                mu = Double.Parse(textBox_mu.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Проверка, что в поле (σ — среднеквадратическое отклонение) введено число.
        /// Если да, строим график по новому введённому значению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_sigma_TextChanged(object sender, EventArgs e)
        {
            if (textBox_check_when_TextChanged(textBox_sigma))
            {
                sigma = Double.Parse(textBox_sigma.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        
        private void textBox_a_TextChanged(object sender, EventArgs e)
        {
            if (textBox_check_when_TextChanged(textBox_a))
            {
                a = Double.Parse(textBox_a.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        private void textBox_b_TextChanged(object sender, EventArgs e)
        {
            if (textBox_check_when_TextChanged(textBox_b))
            {
                b = Double.Parse(textBox_b.Text.Replace('.', ','));
                DrawGraph(xmin_axis_limit, xmax_axis_limit);
            }
        }

        /// <summary>
        /// Переключаем на желаемый график
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton_graphic_2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_graphic_norm_distrib.Checked)
            {
                groupBox_params_norm_distrib.Visible = true;
                groupBox_params_uniform_distrib.Visible = false;
            }
            else
            {
                groupBox_params_norm_distrib.Visible = false;
                groupBox_params_uniform_distrib.Visible = true;
            }
            DrawGraph(xmin_axis_limit, xmax_axis_limit);
        }

        private void numericUpDown_width_ValueChanged(object sender, EventArgs e)
        {
            DrawGraph(xmin_axis_limit, xmax_axis_limit);
        }
    }
}
