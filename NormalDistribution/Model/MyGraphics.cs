﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NormalDistribution.Model
{
    /// <summary>
    /// Класс, в котором хранятся функции для отрисовки
    /// </summary>
    public static class MyGraphics
    {
        /// <summary>
        /// Функция нормального распределения (функция Гаусса)
        /// https://ru.wikipedia.org/wiki/Нормальное_распределение
        /// 
        /// Стандартным нормальным распределением называется
        /// нормальное распределение с математическим ожиданием μ = 0
        /// и стандартным отклонением σ = 1
        /// </summary>
        /// <param name="x"></param>
        /// <param name="mu">μ - математическое ожидание (среднее значение), медиана и мода распределения</param>
        /// <param name="sigma">σ — среднеквадратическое отклонение (σ² — дисперсия) распределения</param>
        /// <returns></returns>
        public static double graphicGauss(double x, double mu = 0, double sigma = 1)
        {
            double a = 1 / (sigma * Math.Sqrt(2 * Math.PI));
            double b = Math.Exp((-1) * (x - mu) * (x - mu) / (2 * sigma * sigma));
            return a * b;
        }

        /// <summary>
        /// Функция непрерывного равномерного распределения
        /// https://ru.wikipedia.org/wiki/Непрерывное_равномерное_распределение
        /// 
        /// Распределение случайной вещественной величины, принимающей значения,
        /// принадлежащие интервалу [a, b], характеризующееся тем, что
        /// плотность вероятности на этом интервале постоянна.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="a">нижняя граница</param>
        /// <param name="b">верхняя граница</param>
        /// <returns></returns>
        public static double graphicUniformDistribution(double x, double a = 0, double b = 1)
        {
            if (x >= a && x <= b)
            {
                return 1 / (b - a);
            }
            else
            {
                return 0;
            }
        }

    }
}
